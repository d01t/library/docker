#!/bin/sh
# Stop on error
set -e
# Install mandatory programs and utilities
export DEBIAN_FRONTEND=noninteractive
apk add --no-cache \
	ca-certificates \
	curl \
	grep \
	openssl \
	sed
# Docker compose
curl --silent -L --fail https://github.com/docker/compose/releases/download/1.24.1/run.sh -o /usr/local/bin/docker-compose
chmod +x /usr/local/bin/docker-compose
