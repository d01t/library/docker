ARG VERSION=latest

FROM docker:${VERSION}

COPY install.sh /root/

RUN /bin/sh /root/install.sh
